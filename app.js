require('dotenv').config()

var express = require('express');
var mysql = require('mysql');

var app = express();

const port = process.env.APP_PORT;
// create connection to mysql server
var con = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE
});

// connect to mysql server
con.connect((err) => {
  if (err) throw err;
  console.log("Connected to mysql database.");
});

app.get('/gpstracker', (req, res) =>{
  // get ip address
  let ip = req.header('x-forwarded-for') || req.socket.remoteAddress;

  // get variables from query
  const {api_key, lat, lng, alt, speed, sat, hdop} = req.query;
  var sql_query = "";

  // check if client provided api key matches with the servers api key
  if(api_key == process.env.KEY_GPS){
    // if one of the variables are undefined then send 400 status code to the client
    if(lat == undefined || lng == undefined || alt == undefined || speed == undefined || sat == undefined || hdop == undefined){
      return res.sendStatus(400);
    }

    console.log(req.query);

    sql_query = "INSERT INTO gps_data (lat, lng, alt, speed, sat, hdop, ip) VALUES (?, ?, ?, ?, ?, ?, ?)";
    var inserts = [lat, lng, alt, speed, sat, hdop, ip];

    // insert variables into the sql_query string
    sql_query = mysql.format(sql_query, inserts);

    // attempt to query mysql server with the sql_query 
    con.query(sql_query, (error, result) =>{
      if (error){
        // on error log the error to console and send 500 status code to client
        console.log(error.code);
        return res.sendStatus(500);
      };
      // on success send client success code
      // console.log(sql_query);
      // console.log(result);
      console.log("New record inserted");
      res.sendStatus(201);
    });
    
  }else{
    // if client sends invalid api key then send 403 status code to the client
    res.sendStatus(403);
    console.log(`Unauthorized access using API key '${api_key}' from IP ${ip}.`);
  }
});

app.get('/temp1', (req, res) =>{
  // get ip address
  let ip = req.header('x-forwarded-for') || req.socket.remoteAddress;

  // get variables from query
  const {api_key, temp} = req.query;
  var sql_query = "";

  // check if client provided api key matches with the servers api key
  if(api_key == process.env.KEY_TEMP1){
    // if temp is undefined then send 400 status code to client client
    if(temp == undefined){
      return res.sendStatus(400);
    }

    console.log(req.query);

    sql_query = "INSERT INTO temp1_data (temp) VALUES (?)";
    var inserts = [temp];

    // insert variables into the sql_query string
    sql_query = mysql.format(sql_query, inserts);

    // attempt to query mysql server with the sql_query 
    con.query(sql_query, (error, result) =>{
      if (error){
        // on error log the error to console and send 500 status code to client
        console.log(error.code);
        return res.sendStatus(500);
      };
      // on success send client success code
      // console.log(sql_query);
      console.log("New record inserted");
      res.sendStatus(201);
    });
    
  }else{
    // if client sends invalid api key then send 403 status code to the client
    res.sendStatus(403);
    console.log(`Unauthorized access using API key '${api_key}' from IP ${ip}.`);
  }
});

app.get('/temp2', (req, res) =>{
  // get ip address
  let ip = req.socket.remoteAddress;

  // get variables from query
  const {api_key, temp} = req.query;
  var sql_query = "";

  // check if client provided api key matches with the servers api key
  if(api_key == process.env.KEY_TEMP2){

    // if temp is undefined then send 400 status code to client client
    if(temp == undefined){
      return res.sendStatus(400);
    }
    
    console.log(req.query);

    // insert variables into the sql_query string
    sql_query = "INSERT INTO temp2_data (temp) VALUES (?)";
    var inserts = [temp];

    sql_query = mysql.format(sql_query, inserts);

    // attempt to query mysql server with the sql_query 
    con.query(sql_query, (error, result) =>{
      if (error){
        // on error log the error to console and send 500 status code to client
        console.log(error.code);
        return res.sendStatus(500);
      };
      // on success send client success code
      // console.log(sql_query);
      console.log("New record inserted");
      res.sendStatus(201);
    });
    
  }else{
    // if client sends invalid api key then send 403 status code to the client
    res.sendStatus(403);
    console.log(`Unauthorized access using API key '${api_key}' from IP ${ip}.`);
  }
});

// start listening 
app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});